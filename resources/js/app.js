require('./bootstrap');
window.Vue = require('vue');

import App from './App.vue';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import router from './router';
import store from './store';
import axios from 'axios';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false

const app = new Vue({
    el: '#app',
    store,
    router: router,
    created () {
        const userInfo = localStorage.getItem('user')
        if (userInfo) {
          const userData = JSON.parse(userInfo)
          this.$store.commit('setUserData', userData)
        }
        axios.interceptors.response.use(
          response => response,
          error => {
            if (error.response.status === 401) {
              this.$store.dispatch('logout')
            }
            return Promise.reject(error)
          }
        )
      },
    render: h => h(App),
});