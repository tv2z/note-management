<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\User;
use Illuminate\Support\Facades\Hash;

Route::post('/login', function (Request $request) {
    $data = $request->validate([
        'email' => 'required|email',
        'password' => 'required'
    ]);

    $user = User::where('email', $request->email)->first();

    if (!$user || !Hash::check($request->password, $user->password)) {
        return response([
            'message' => ['These credentials do not match our records.']
        ], 404);
    }

    $token = $user->createToken('my-app-token')->plainTextToken;

    $response = [
        'user' => $user,
        'token' => $token
    ];

    return response($response, 201);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// step 3 route for notes application.

use App\Http\Request as HttpRequest;

Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

Route::get('notes', 'NoteController@index');
Route::group(['prefix' => 'note'], function () {
    Route::post('notes/create', 'NoteController@add');
    Route::get('edit/{id}', 'NoteController@edit');
    Route::post('update/{id}', 'NoteController@update');
    Route::delete('delete/{id}', 'NoteController@delete');
});