
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "note-index" */ '../views/Note/Index.vue')
  },
  {
    name: 'note.index',
    path: '/notes',
    component: () => import(/* webpackChunkName: "note-index" */ '../views/Note/Index.vue')
  },
{
    name: 'notes.create',
    path: '/notes/create',
    component: () => import(/* webpackChunkName: "note-create" */ '../views/Note/Create.vue')
},
{
    name: 'notes.edit',
    path: '/notes/:id/update',
    component: () => import(/* webpackChunkName: "note-update" */ '../views/Note/Update.vue')
},
  {
    name: 'login',
    path: '/login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


export default router
