<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'majid amer',
            'email' => 'majidamer@hotmail.com',
            'password' => Hash::make('password')
        ]);
    }
}
